﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    public class Category
    {
        public int  IdCategory {get; set;}
        public string Name {get; set; }
        public int IdUser { get; set; }

        public Category() { }
    }
}
