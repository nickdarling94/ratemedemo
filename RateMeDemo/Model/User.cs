﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    public class User
    {
        public int IdUser { get; set; }
        public int Type { get; set; } //0 is regular user, 1 is administrator
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public User() { }

    }
}
