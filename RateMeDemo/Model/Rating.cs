﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    public class Rating
    {
        public int IdUser { get; set; }
        public int IdObjectInstance { get; set; }
        public int Grade { get; set; }

        public Rating() { }
    }
}
