﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.Model
{
    public class ObjectInstance
    {
        public Dictionary<string,string> keyValuePairs { get; set; }
        public int IdObject { get; set; }

        public ObjectInstance() {   }
    }
}