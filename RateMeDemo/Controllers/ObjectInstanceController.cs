﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RateMeDemo.DataProvider;
using RateMeDemo.Model;


namespace RateMeDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ObjectInstanceController : ControllerBase
    {
        // GET: api/Object
        [HttpGet]
        public JArray Get()
        {
            JArray ar = new JArray();
            List<Dictionary<string, string>> dictionary = new List<Dictionary<string, string>>();
            dictionary = ObjectInstanceDataProvider.GetObjectInstances();

            for(int i=0;i<dictionary.Count;i++)
            {
                Dictionary<string, string> dictionary_element = new Dictionary<string, string>();
                dictionary_element = dictionary.ElementAt(i);
                JObject jobj = new JObject();

                for (int j=0; j<dictionary_element.Count;j++)
                {
                    jobj.Add(dictionary_element.ElementAt(j).Key, dictionary_element.ElementAt(j).Value);


                }
                ar.Add(jobj);
            }
            return ar;
        }

        // GET: api/Object/5
        [HttpGet("{id}", Name = "Get")]
        public JObject Get(int id)
        {
            ObjectInstance u = DataProvider.ObjectInstanceDataProvider.GetObjectInstance(id);
            JObject j = new JObject();
            for (int i = 0; i < u.keyValuePairs.Count; i++)
                j.Add(u.keyValuePairs.ElementAt(i).Key,u.keyValuePairs.ElementAt(i).Value);
            return j;
        }

        // POST: api/Object
        [HttpPost]
        public void Post([FromBody] IDictionary<string,string> value)
        {
            DataProvider.ObjectInstanceDataProvider.AddObjectInstance(value);
        }

        // PUT: api/Object/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]  IDictionary<string, string> value)
        {
            DataProvider.ObjectInstanceDataProvider.updateObjectInstance(id,value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            DataProvider.ObjectInstanceDataProvider.DeleteObjectInstance(id);
        }
    }
}
