﻿using Microsoft.AspNetCore.Mvc;

namespace RateMeDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        //GET: api/Rating
        [HttpGet]
        public string Get()
        {
            //List<User> users = new List<User>();
            //users = UserDataProvider.GetUsers();
            //return users;
            return "void";
        }

        //GET: api/Rating/5
        [HttpGet("{id}", Name = "GetRating")]
        public void Get(int id)
        {
            // return "value";
        }

        //POST: api/Rating
        [HttpPost]
        public void Post(string value)
        {
        }

        //PUT: api/Rating/5
        [HttpPut("{id}")]
        public void Put(int id, string value)
        {
        }

        //DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
