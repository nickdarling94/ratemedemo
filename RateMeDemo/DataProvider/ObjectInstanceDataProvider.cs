﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class ObjectInstanceDataProvider
    {
        #region GET OBJECTINSTANCE
        /// <summary>
        /// Retrieves the a ObjectInstance with specified id
        /// MATCH(n) WHERE ID(n) = 2 RETURN n
        /// </summary>
        /// <returns></returns>
        internal static ObjectInstance GetObjectInstance(int id)
        {
            ObjectInstance u = new ObjectInstance();

            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Dictionary<string,string>>()).Results.ToList();


            if (query.Count != 0)
            {
                u.keyValuePairs = query[0];
                return u;
            }
            else
                return null;
        }

        /// <summary>
        /// Retrieves all ObjectInstances from DB
        /// MATCH (n:User) RETURN n
        /// </summary>
        /// <returns></returns>
        internal static List<Dictionary<string,string>> GetObjectInstances()
        {
            ConnectDb.Connect();
        
            return   ConnectDb.client.Cypher.Match("(n)")
               .Return((n) => n.As<Dictionary<string, string>>()).Results.ToList();
        }
        #endregion

        #region POST OBJECTINSTANCE
        /// <summary>
        /// Creates a new User in DB
        /// CREATE (n:User {password:'1234', username:'NewUser', email:'new@usermail.com'})
        /// </summary>
        /// <returns></returns>
        internal static void AddObjectInstance(IDictionary<string,string> u)
        {
               ConnectDb.Connect();
               IEnumerable<int> id=  ConnectDb.client.Cypher
                                 .Create("(n)")
                                 .Return<int>("ID(n)").Results;
            int id_1 = id.ElementAt(0);

            for (int i = 0; i < u.Count; i++)
                ConnectDb.client.Cypher.Match("(n)").Where("ID(n)="+id_1).                    
                    Set("n."+u.ElementAt(i).Key+"= '"+u.ElementAt(i).Value+"'").ExecuteWithoutResults();

        }
        #endregion

        #region PUT OBJECTINSTANCE
        /// <summary>
        /// Updates an existing OBJECTINSTANCE in DB
        /// MATCH (u:User) WHERE ID(u) = 1 SET u.username = 'newUsername'
        /// MATCH (u:User) WHERE ID(u) = 7 SET u = { username: 'Michael' }
        /// </summary>
        /// <returns></returns>
        internal static void updateObjectInstance(int id, IDictionary<string, string> u)
        {
            ConnectDb.Connect();
            

            for (int i = 0; i < u.Count; i++)
                ConnectDb.client.Cypher.Match("(n)").Where("ID(n)=" + id).
                    Set("n." + u.ElementAt(i).Key + "= '" + u.ElementAt(i).Value + "'").ExecuteWithoutResults();
        }
        #endregion

        #region DELETE OBJECTINSTANCE
        /// <summary>
        /// Deletes an existing User in DB
        /// MATCH(n: Person { name: 'UNKNOWN' }) DELETE n
        /// </summary>
        /// <returns></returns>
        internal static void DeleteObjectInstance(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n)")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n")
              .ExecuteWithoutResults();

            //.Where((User n) => n.idUser == id)
        }
        #endregion  
    }
}
