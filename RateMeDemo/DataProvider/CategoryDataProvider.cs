﻿using RateMeDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RateMeDemo.DataProvider
{
    public class CategoryDataProvider
    {
        #region GET CATEGORY
        internal static Category GetCategory(int id)
        {
            Category c = new Category();
            ConnectDb.Connect();

            var query = ConnectDb.client.Cypher.Match("(n:Category)").Where("ID(n) = {id}").
                WithParam("id", id).Return((n) => n.As<Category>()).Results.ToList();

            if (query.Count != 0)
            {
                c = query[0];
                return c;
            }
            else return null;

        }
        internal static List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();

            ConnectDb.Connect();

            return categories = ConnectDb.client.Cypher.Match("(n:Category)")
               .Return((n) => n.As<Category>()).Results.ToList();
        }
        #endregion

        #region POST CATEGORY

        internal static void AddCategory(Category u)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
               .Create("(n:Category {c})")
               .WithParam("c", u)
               .ExecuteWithoutResults();
        }
        #endregion

        #region DELETE CATEGORY
        internal static void DeleteCategory(int id)
        {
            ConnectDb.Connect();
            ConnectDb.client.Cypher
              .OptionalMatch("(n:Category)-[r]->()")
              .Where("ID(n) = {id}")
              .WithParam("id", id)
              .Delete("n,r")
              .ExecuteWithoutResults();

            //.Where((User n) => n.idUser == id)
        }
        #endregion

        #region UPDATE CATEGORY

        internal static void UpdateCategory(int id, Category c)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("id", id);
            dict.Add("c", c);
            ConnectDb.Connect();
            ConnectDb.client.Cypher
                .Match("(n:Category)")
                .Where("ID(n) = {id}")
                .Set("n:Category {c})")
                .WithParams(dict)
                .ExecuteWithoutResults();
        }
        #endregion

    }
}
